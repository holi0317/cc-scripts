local pretty = require("cc.pretty")
local logger = require("logger")
local Altar = require("altar")

-- Constants
local SLEEP_INTERVAL = 1
local DEFAULT_SLATE_TARGET_QUANTITY = 64
local ITEMS = {
	stone = "minecraft:stone",
	T1Slate = "bloodmagic:blankslate",
	T2Slate = "bloodmagic:reinforcedslate",
	T3Slate = "bloodmagic:infusedslate",
	T4Slate = "bloodmagic:demonslate",
}

-- Setup logger
local log = logger.new()
log.level = logger.logLevels.DEBUG

--#region Slate class

---A blood slate crafting recipe
---@class Slate
---@field item_name string
---@field blood_volume number
---@field ingredient string
---@field target_quantity number
local Slate = {}

---@param item_name string
---@param blood_volume number
---@param ingredient string
---@param target_quantity number
function Slate:new(item_name, blood_volume, ingredient, target_quantity)
	local o = {}
	setmetatable(o, self)
	self.__index = self

	o.item_name = item_name
	o.blood_volume = blood_volume
	o.ingredient = ingredient
	o.target_quantity = target_quantity

	return o
end

function Slate:__tostring()
	return self.item_name
end

---Check if current slate is craftable or not.
---@param blood_level number Bucket (in mb) of blood available in altar now
---@param item_counts table<string, number> Table of item counts in inventory.
---  Key is item name and value is number
---@return boolean True if the slate is craftable, false otherwise
function Slate:craftable(blood_level, item_counts)
	local count = item_counts[self.item_name] or 0
	local ingredient_count = item_counts[self.ingredient] or 0

	log:debug("Determining if %s is craftable", self)

	if count >= self.target_quantity then
		log:debug(
			"Slate already reached target quantity. Refuse to craft. Count = %s, target quantity = %s",
			count,
			self.target_quantity
		)
		return false
	end

	if blood_level < self.blood_volume then
		log:debug(
			"Insufficient blood for crafting. Refuse to craft. Blood level = %s, Required = %s",
			blood_level,
			self.blood_volume
		)
		return false
	end

	if ingredient_count < 0 then
		log:debug(
			"Insufficient ingredient for crafting. Refuse to craft. Ingredient = %s, Count = %s",
			self.ingredient,
			ingredient_count
		)
		return false
	end

	return true
end

--#endregion

--#region Setting class

---@class Setting
---@field slates Slate[]
---@field _inventory_type string
---@field _inventory table Cache of `inventory()` output
---@field _redstone_control_side string|nil
local Setting = {}

function Setting:new()
	local o = {}
	setmetatable(o, self)
	self.__index = self

	Setting._define_setting()
	Setting._load_setting()

	o._inventory_type = settings.get("bmslate.inventory_type")
	o._redstone_control_side = settings.get("bmslate.redstone_control_side")

	local slate_target_quantity = settings.get("bmslate.slate_target_quantity")
	o.slates = Setting._parse_slate_target_quantity(slate_target_quantity)

	return o
end

function Setting._define_setting()
	settings.define("bmslate.inventory_type", {
		description = "Type of inventory (in terms of peripheral name) to use for storing stone and slate",
		default = "storagedrawers:controller",
	})
	settings.define("bmslate.redstone_control_side", {
		description = "Side for toggling machine's activation. When redstone is powered on this side means turn off the program. Empty string/nil to ignore redstone",
		default = "top",
	})
	settings.define("bmslate.slate_target_quantity", {
		description = "Target quantity for each slate. Use comma-separated number for each tier or use 1 number only for setting all slate to have same target quantity",
		default = DEFAULT_SLATE_TARGET_QUANTITY,
	})
end

function Setting._load_setting()
	log:info("Loading settings on .settings")
	if not settings.load() then
		log:warn("Failed to load settings. Using default settings instead")
		return
	end
end

---Parse slate target quantity
function Setting._parse_slate_target_quantity(value)
	local slates = {
		Slate:new(ITEMS.T1Slate, 1000, ITEMS.stone, DEFAULT_SLATE_TARGET_QUANTITY),
		Slate:new(ITEMS.T2Slate, 2000, ITEMS.T1Slate, DEFAULT_SLATE_TARGET_QUANTITY),
		Slate:new(ITEMS.T3Slate, 5000, ITEMS.T2Slate, DEFAULT_SLATE_TARGET_QUANTITY),
		Slate:new(ITEMS.T4Slate, 15000, ITEMS.T3Slate, DEFAULT_SLATE_TARGET_QUANTITY),
	}

	if value == nil then
		return slates
	end

	if type(value) == "number" then
		log:debug("Get target slate quantity a single number")
		for _, slate in ipairs(slates) do
			slate.target_quantity = value
		end

		return slates
	end

	if type(value) ~= "table" then
		log:warn("Unknown bmslate.slate_target_quantity type %s. Use default value instead", type(value))
		return slates
	end

	if #value < #slates then
		log:warn(
			"Number of slate target quantity too few. Expected to have %s elements. Got %s elements only. Unspecified ones will use default value.",
			#slates,
			#value
		)
	end

	for index, val in ipairs(value) do
		if index > #slates then
		else
			slates[index].target_quantity = val
		end
	end

	return slates
end

---Check if the app should enable or not at current tick
---@return boolean
function Setting:is_enabled()
	local side = self._redstone_control_side
	if side == nil or side == "" then
		log:debug("Redstone is disabled")
		return true
	end

	log:debug("Checking redstone on side %s", side)

	return not redstone.getInput(side)
end

---Get peripheral for inventory.
---This will cache the peripheral inside the instance.
---@return table
function Setting:inventory()
	if self._inventory ~= nil then
		return self._inventory
	end

	-- Get inventory peripherals
	log:info("Finding inventory peripheral. Type = %s", self._inventory_type)

	local inventories = { peripheral.find(self._inventory_type) }
	if #inventories == 0 then
		error("Cannot find stone and slate inventory. Did you connect the inventory with modem?")
	end
	if #inventories > 1 then
		log:warn("Multiple stone and slate inventory found. Using %s", peripheral.getName(inventories[1]))
	end

	self._inventory = inventories[1]

	log:info("Found inventory. Name = %s", peripheral.getName(self._inventory))
	return self._inventory
end

--#endregion

--#region App class definition

---@class App
---@field setting Setting
---@field altar Altar
---@field crafting Slate|nil
local App = {}

---@param setting Setting
function App:new(setting)
	local o = {}
	setmetatable(o, self)
	self.__index = self

	log:info("Booting up")

	o.crafting = nil
	o.setting = setting

	-- Get altar peripheral
	o.altar = Altar:new()

	-- Get inventory peripherals on boot
	setting:inventory()

	-- Remove existing item in altar on boot
	log:info("Clearing altar content")
	o.altar:clear(setting:inventory())

	return o
end

function App:_try_end_crafting()
	if self.crafting == nil then
		return
	end

	log:info("Check crafting status of %s", self.crafting.item_name)

	local item = self.altar:item()
	if item == nil then
		log:warn(
			"Expected to have item %s or %s in altar but found nothing. Abort crafting",
			self.crafting.ingredient,
			self.crafting.item_name
		)

		self.crafting = nil
		return
	end

	if item.name ~= self.crafting.ingredient and item.name ~= self.crafting.item_name then
		log:warn(
			"Expected to have item %s or %s in altar but found %s. Abort crafting",
			self.crafting.ingredient,
			self.crafting.item_name,
			item.name
		)

		self.crafting = nil
		return
	end

	if item.name == self.crafting.ingredient then
		-- Still crafting. Noop
		return
	end

	log:info("Craft of %s completed. Pulling out", item.name)
	self.altar:clear(self.setting:inventory())
	self.crafting = nil
end

function App:update()
	if not self.setting:is_enabled() then
		self.crafting = nil
		log:info("Received pause redstone signal. Sleeping")
		return
	end

	self:_try_end_crafting()
	if self.crafting ~= nil then
		log:info("Crafting of %s have not completed. Waiting for next process tick.", self.crafting.item_name)
		return
	end

	local altar_item = self.altar:item()
	if altar_item ~= nil then
		log:warn("Expected altar to be empty but got %s. Please remove the item manually.", altar_item.name)
		return
	end

	-- Count items in inventory
	local item_counts = {}
	local slot_map = {} -- Table for the first slot in `inventory` containing the item (as key)
	for slot, item in pairs(self.setting:inventory().list()) do
		local accumulate = item_counts[item.name] or 0
		item_counts[item.name] = accumulate + item.count

		if slot_map[item.name] == nil then
			slot_map[item.name] = slot
		end
	end

	-- Get current blood volume
	local blood_level = self.altar:blood_level()

	log:info("Finding craftable slate. Blood level = %s, Items count = %s", blood_level, pretty.pretty(item_counts))

	-- Look for crafting candidate
	for _, slate in ipairs(self.setting.slates) do
		if slate:craftable(blood_level, item_counts) then
			log:info("Crafting %s", slate.item_name)

			self.setting:inventory().pushItems(self.altar:string(), slot_map[slate.ingredient], 1)

			self.crafting = slate
			return
		end
	end

	log:info("No possible crafting candidate found")
end

function App:start()
	while true do
		self:update()
		sleep(SLEEP_INTERVAL)
	end
end
--#endregion

local function main()
	local setting = Setting:new()

	local app = App:new(setting)
	app:start()
end

main()
