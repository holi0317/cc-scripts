--[[
    Logger v2 module for ComputerCraft by AlexDevs
    (c) 2021 AlexDevs

    The MIT License
    Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

    Source: https://gist.github.com/Ale32bit/df1d9d455b0b82702308099ba4ea2e0d
]]
local expect = require("cc.expect").expect

local function enum(tbl)
	local output = {}
	for k, v in ipairs(tbl) do
		output[k] = v
		output[v] = k
	end
	return output
end

local Logger = {}

-- Log levels starting at 1
Logger.logLevels = enum({
	"TRACE", -- 1
	"DEBUG", -- 2
	"INFORMATION", -- 3
	"WARNING", -- 4
	"ERROR", -- 5
	"CRITICAL", -- 6
})

Logger.dateFormats = {
	TIME = "%H:%M:%S.%%03d", -- default
	DATE = "%Y-%m-%d",
	DATETIME = "%Y-%m-%d %H:%M:%S.%%03d",
}

-- Display options for logs
Logger.formattedLevels = {
	{
		color = colors.gray,
		name = "TRACE",
	},
	{
		color = colors.lightGray,
		name = "DEBUG",
	},
	{
		color = colors.white,
		name = "INFO",
	},
	{
		color = colors.yellow,
		name = "WARN",
	},
	{
		color = colors.red,
		name = "ERROR",
	},
	{
		color = colors.purple,
		name = "CRIT",
	},
}

Logger.providers = {
	print = function(level, date, source, message) -- Default provider
		local color = term.getTextColor()
		term.setTextColor(level.color)
		print(string.format("%s %s\n\t[%s] %s", date, source, level.name, message))
		term.setTextColor(color)
	end,
	printMonochrome = function(level, date, source, message)
		print(string.format("%s %s\n\t[%s] %s", date, source, level.name, message))
	end,
	file = function(level, date, source, message)
		local f = fs.open("logs/" .. os.date("%Y%m%d") .. ".log", "a")
		f.write(string.format("%s %s [%s] %s\n", date, source, level.name, message))
		f.close()
	end,
}

-- Create a new instance of the Logger
function Logger.new()
	local logger = setmetatable({}, { __index = Logger })
	logger.level = Logger.logLevels.INFORMATION
	logger.dateFormat = Logger.dateFormats.TIME
	logger.providers = {}

	logger:addProvider(Logger.providers.print)

	return logger
end

-- Add functions to output logs, includes print by default
function Logger:addProvider(log)
	expect(1, log, "function")
	table.insert(self.providers, log)
end

-- Generic log function
-- logLevel is the index of the log level in Logger.levels
-- If self.level is higher than the logLevel, the log will be ignored
function Logger:log(logLevel, message, ...)
	expect(1, logLevel, "number")
	if logLevel < self.level then
		return
	end

	level = Logger.formattedLevels[logLevel or Logger.logLevels.INFORMATION]
	message = tostring(message):format(...)
	local date = os.date(self.dateFormat):format(os.epoch("utc") % 1000)

	local source = debug.getinfo(3, "S").source

	for _, provider in ipairs(self.providers) do
		assert(pcall(provider, level, date, source, message))
	end
end

-- Specific log functions

function Logger:trace(message, ...)
	self:log(Logger.logLevels.TRACE, message, ...)
end

function Logger:debug(message, ...)
	self:log(Logger.logLevels.DEBUG, message, ...)
end

function Logger:info(message, ...)
	self:log(Logger.logLevels.INFORMATION, message, ...)
end

function Logger:warn(message, ...)
	self:log(Logger.logLevels.WARNING, message, ...)
end

function Logger:error(message, ...)
	self:log(Logger.logLevels.ERROR, message, ...)
end

function Logger:critical(message, ...)
	self:log(Logger.logLevels.CRITICAL, message, ...)
end

setmetatable(Logger, { __call = Logger.new })
return Logger
