# Computercraft scripts

Collection of scripts for computercraft, the mod for game Minecraft.

## `bmslate.lua`

Automate slate creation from blood magic. Tested on minecraft 1.6.5, pack Enigmatica 6.

## License

MIT
