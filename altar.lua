local logger = require("logger")

-- Constants
local ALTAR_PERIPHERAL_TYPE = "bloodmagic:altar"
local BLOOD_LIQUID_NAME = "bloodmagic:life_essence_fluid"

local log = logger.new()
log:addProvider(logger.providers.file)

---Wrapper for blood altar
---@class Altar
---@field peripheral table
local Altar = {}

function Altar:new()
	---@type Altar
	local o = {}
	setmetatable(o, self)
	self.__index = self

	log:info("Finding blood altar peripheral")
	local altars = { peripheral.find(ALTAR_PERIPHERAL_TYPE) }
	if #altars == 0 then
		error("Cannot find blood altar peripheral. Is the altar connected?")
	end
	if #altars > 1 then
		log:warn("Multiple altar found. Using %s", peripheral.getName(altars[1]))
	end

	o.peripheral = altars[1]
	log:info("Using blood altar %s", o:string())

	return o
end

---Get peripheral name of this altar.
---@return string
function Altar:string()
	return peripheral.getName(self.peripheral)
end

---Get peripheral name of this altar.
---@return string
function Altar:__tostring()
	return self:string()
end

---Get item detail of the item inside the altar
---@return table|nil
function Altar:item()
	return self.peripheral.getItemDetail(1)
end

---Clear altar content and push to given inventory. If the altar does not have item inside, noop.
---@param push_to string|table Inventory name or inventory peripheral to push to
function Altar:clear(push_to)
	local item = self:item()
	if item == nil then
		return
	end

	if type(push_to) == "table" then
		push_to = peripheral.getName(push_to)
	end

	log:info("Pulling out item from altar to %s. Item = %s", push_to, item.name)

	self.peripheral.pushItems(push_to, 1)

	log:info("Pulled out %s to inventory %s", item.name, push_to)

	item = self:item()
	if item ~= nil then
		log:warn("Failed to clear altar content. Is inventory full?")
	end
end

---Get amount of blood in altar in terms of mb.
---If the amount cannot be get, 0 will be returned and warning will be emitted to logger.
---@return number
function Altar:blood_level()
	local tanks = self.peripheral.tanks()
	if #tanks == 0 then
		log:warn("The connected altar does not have tank. Is the altar correctly connected?")
		return 0
	end

	local tank = tanks[1]
	if tank == nil then
		return 0
	end

	if tank.name ~= BLOOD_LIQUID_NAME then
		log:warn("Expected altar have tank of %s. Got %s", BLOOD_LIQUID_NAME, tank.name)
		return 0
	end

	return tank.amount
end

return Altar
